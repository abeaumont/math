Let's name the numbers $`a, b, c, d, e`$ and assume, without loss of generality that $`a < b < c < d < e`$.

Let's initially fix $`a, b, c, d`$ to their minimal values: $`a=1,b=3,c=5,d=7`$. In this case, $`e`$ can take any value in $`[9,18]`$, that is, there are 10 possible values for $`e`$.

Now, let's keep $`a, b, c`$ fixed to their minimal values. We can see that $`d`$ can take any value in $`[7,16]`$ and $`e`$ can take any value in $`[d+2,18]`$, that is:

```math
\sum_{d=7}^{16}18-(d+2)+1 = \sum_{d=7}^{16}17-d = \sum_{d=0}^{9}10-d = \sum_{i=1}^{10}i = \binom{11}{2}
```

Similarly, if we keep $`a, b`$ fixed to their minimal values, we can see that $`c`$ can take any value in $`[5,14]`$ and $`d, e`$ get their range restricted accordingly:

```math
\sum_{c=5}^{14}\sum_{d=c+2}^{16}17-d = \sum_{c=5}^{14}\sum_{d=c+2-7}^{16-7}17-7-d = \sum_{c=5}^{14}\sum_{d=c-5}^{9}10-d = \sum_{c=5}^{14}\sum_{d=c-5}^{9}10-d = \sum_{c=1}^{10}\sum_{d=c-1}^{9}10-d = \sum_{c=1}^{10}\sum_{i=1}^{c}i = \sum_{c=1}^{10}\binom{c+1}{2} = \binom{12}{3}
```

If we generalize this to all the numbers, we have:

```math
\sum_{a=1}^{10}\sum_{b=a+2}^{12}\sum_{c=b+2}^{14}\sum_{d=c+2}^{16}17-d = \sum_{a=1}^{10}\sum_{b=a+2}^{12}\sum_{c=b+2}^{14}\sum_{i=1}^{15-c}i = \sum_{a=1}^{10}\sum_{k=1}^{a}\sum_{j=1}^{k}\sum_{i=1}^{j}i = \sum_{a=1}^{10}\sum_{k=1}^{a}\sum_{j=1}^{k}\binom{j+1}{2} = \sum_{a=1}^{10}\sum_{k=1}^{a}\binom{k+2}{3} = \sum_{a=1}^{10}\binom{a+3}{4} = \binom{14}{5} = 2002
```
