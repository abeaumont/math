There are 1000 pairs of consecutive integers in the set: $`(1000, 1001) ... (1999,2000)`$

All the numbers whose last digit is in the range $`[5,8]`$ requires a carry due to the last digit addition, while the rest of numbers do not require a carry due to first (counting from right to left) digit. That means that 400 pairs in the set will require a carry due to first digit.

Similarly, all the numbers whose last 2 digits are in the range $`[50,98]`$ require a carry due to last 2 digit addition. Of those, 20 digits will also require a carry due to the first digit, which means that there will be $`(49-20)*10 = 290`$ numbers that will require a carry in the second digit but not in the first one.

Similarly, all the pairs whose first number is in the range $`1500,1999`$ require a carry in the third digit. If we exclude those numbers that also require a carry due to first and second digits, we have that $`499 - 290/2 - 400/2 = 154`$ numbers require a carry due to the third digit but not the first or second ones.

So, summing all the numbers that require a carry, we have a total of $`400+290+154=844`$ numbers requiring a carry. Thus, the total number of consecutive pairs that don't require carrying when added is $`1000 - 844 = 156`$.
