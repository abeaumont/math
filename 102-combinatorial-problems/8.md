The different orders to put socks and shoes without the constraint is $`16!`$, which is the number of permutations of a sequence of length 16.  

For each leg, the number of permutations in which the sock goes before the corresponding sock (and viceversa) is $`\frac{1}{2} = 2^{-1}`$.  
So we have to discard a half of the (remaining) permutations for each leg, having finally $`2^{-8}`$ valid permutations of the original $`16!`$.  

That is, the number of different orders is $`\frac{16!}{2^8}`$.
