To have both sequences $`d_4d_5d_6`$ and $`d_5d_6d_7`$ equal to the prefix sequence:
- $`d_1 \eq d_4 \eq d_5`$
- $`d_2 \eq d_5 \eq d_6`$
- $`d_3 \eq d_6 \eq d_7`$

That is, all the digits must be the same. We have 10 sequences of this type ($`d_i \in [0,9] \forall i`$).
There are, in total, 1000 distinct prefix sequences: $`[000,999]`$ So, we have 990 prefix sequences in which $`d_4d_5d_6 \neq d_5d_6d_7`$.

For each prefix sequence, there are:
- 10 sequences in which $`d_1d_2d_3 \eq d_4d_5d_6`$ (with `d7` in range $`[0,9]`$).
- 10 sequences in which $`d_1d_2d_3 \eq d_5d_6d_7`$ (with `d4` in range $`[0,9]`$).
- Possibly (10 out of 1000 cases), 1 sequence in which $`d_1d_2_d3 \eq d_4d_5d_6 \eq d_5d_6d_7`$.

So, summing everything, we have $`1000 * (10+10) - 10 = 19990`$
