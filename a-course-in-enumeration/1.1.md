The subsets of the union of $`t`$ disjoint sets $`S_i`$ in which at most one element from each $`S_{i}`$ is contained can be expressed as a product of $`t`$ sets, where each coordinate can contain $`a_i+1`$ elements (we can pick any element from the set $`S_i`$, or no element, that is $`|S_i|+1 = a_i+1`$.  
If we call this product of sets $`S`$, by the rule of product we get $`|S| = \prod_{i=1}^{t}(a_i+1)`$.

For $`t(n) = \prod_{i=1}^{t}(a_i+1)`$:

```math
t(n)\nmid 2 \implies (a_i+1)\nmid 2 \, \forall i \implies a_i \mid 2 \, \forall i
```

That is, all the prime factors of $`n`$ are even, and
each $`a_i`$ can be expressed as $`a_i = 2k_i`$ which means that:

```math

n = p_1^{a_1}p_2^{a_2} ... p_n^{a_n} = p_1^{2k_1}p_2^{2k_2} ... p_n^{2k_n} = (p_1^{k_1}p_2^{k_2} ... p_n^{k_n})^2
```

That is, a perfect square.
